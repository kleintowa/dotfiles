#!/usr/local/bin/bash

# install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# install bundle (Brewfile with all the stuff)
brew bundle

# Link .files to  dotfiles via relative path and var
# https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within?noredirect=1&lq=1
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
for DOTFILE in $(ls $DIR/dots)
do
  ln -s $DIR/dots/$DOTFILE ~/.$DOTFILE
done
